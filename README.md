[![pipeline status](https://gitlab.com/marko453/sqr-lab-1-solution/badges/master/pipeline.svg)](https://gitlab.com/marko453/sqr-lab-1-solution/-/commits/master)
[![coverage report](https://gitlab.com/marko453/sqr-lab-1-solution/badges/master/coverage.svg)](https://gitlab.com/marko453/sqr-lab-1-solution/-/commits/master)

# Lab 1 - Introduction to the Quality Gates

Student name: **Marko Pezer** \
Student group: **BS18-SE-01**

You can check my application [HERE](https://heroku-sqr-lab1-staging.herokuapp.com/hello).
